import _ from "lodash";
import { ExportJsonCsv } from "react-export-json-csv";

export default ({ list, setList, setInput, setLinesCount, loading }) => {
  const columns = [
    {
      name: "翻譯地區",
      title: "翻譯地區",
      dataIndex: "district",
      key: "district",
    },
    {
      name: "翻譯地址",
      title: "翻譯地址",
      dataIndex: "formatted_address",
      key: "formatted_address",
    },
    {
      name: "原有地址",
      title: "原有地址",
      dataIndex: "origin_address",
      key: "origin_address",
    },
    { name: "Lat", title: "Lat", dataIndex: "lat", key: "lat" },
    { name: "Lng", title: "Lng", dataIndex: "lng", key: "lng" },
  ];

  return (
    <div>
      {/* End result count(success/failed) */}
      <table className="cleaning-table" data-toggle="table">
        <thead>
          <tr>
            <th>翻譯地區</th>
            <th>翻譯地址</th>
            <th>原地址</th>
            <th>Lat</th>
            <th>Lng</th>
          </tr>
        </thead>
        <tbody>
          {list &&
            list.map((item, index) => {
              return (
                <tr key={`${item.lat},${item.lng}_${index}`}>
                  <td>{item.district}</td>
                  <td>{item.formatted_address}</td>
                  <td style={{ width: "33%" }}>{item.origin_address}</td>
                  <td>{item.lat}</td>
                  <td>{item.lng}</td>
                </tr>
              );
            })}
        </tbody>
      </table>

      {!loading && (
        <div className="row">
          <div className="col-3">
            <button
              type="button"
              className="theme-btn theme-btn-two"
              onClick={() => {
                setList();
                setInput("");
                setLinesCount(0);
                window.scrollTo(0, 0);
              }}
            >
              再次輸入
            </button>
          </div>

          <div className="col-3">
            <ExportJsonCsv
              className="theme-btn theme-btn-five"
              headers={columns}
              items={list}
            >
              下載CSV
            </ExportJsonCsv>
          </div>
        </div>
      )}
    </div>
  );
};
