import AddressCleaningForm from "./address-cleaning-form";

export default ({
  list,
  setList,
  linesCount,
  setLinesCount,
  input,
  setInput,
  onClickConvert,
  loading,
}) => {
  return (
    <>
      <div className="fancy-hero-one address-cleaning-intro">
        <div className="container">
          <div className="row">
            <div className="col-lg-9 m-auto">
              <h1 className="font-rubik" style={{ fontSize: 36 }}>
                批量香港地址轉座標
              </h1>
              <p className="sub-heading">
                批次輸入大量地址，一天內就可以完成數十萬地址定位
              </p>
            </div>
          </div>
          {/* End .row */}
        </div>
        {/* Form */}
        <div className="container">
          <div className="form-style-light form-style-cleaning">
            <AddressCleaningForm
              loading={loading}
              input={input}
              setInput={setInput}
              list={list}
              setList={setList}
              linesCount={linesCount}
              setLinesCount={setLinesCount}
              onClickConvert={onClickConvert}
            />
          </div>
        </div>
        {/* End .container */}
        <div className="bubble-one"></div>
        <div className="bubble-two"></div>
        <div className="bubble-three"></div>
        <div className="bubble-four"></div>
        <div className="bubble-five"></div>
        <div className="bubble-six"></div>
      </div>
      <div className="contact-us-light pt-70 pb-70 md-pt-90 md-pb-80">
        <div className="bubble-one"></div>
        <div className="bubble-two"></div>
        <div className="bubble-three"></div>
        <div className="bubble-four"></div>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-4 col-sm-6">
              <div className="address-info">
                <div className="icon">
                  <img src="images/icon/15.svg" alt="icon" />
                </div>
                <div className="title">上傳</div>
                <p className="font-rubik">
                  建立一個地址列表並將其保存為 CSV 文件
                </p>
              </div>
              {/* /.address-info */}
            </div>
            {/* End .col */}

            <div className="col-md-4 col-sm-6">
              <div className="address-info">
                <div className="icon">
                  <img src="images/icon/16.svg" alt="icon" />
                </div>
                <div className="title">驗證</div>
                <p className="font-rubik">
                  利用我們的地址驗證服務批量處理大量地址
                </p>
              </div>
              {/* /.address-info */}
            </div>
            {/* End .col */}
            <div className="col-md-4 col-sm-6">
              <div className="address-info">
                <div className="icon">
                  <img src="images/icon/17.svg" alt="icon" />
                </div>
                <div className="title">下載</div>
                <p className="font-rubik">下載驗證後的地址座標列表</p>
              </div>
              {/* /.address-info */}
            </div>
            {/* End .col */}
          </div>
        </div>
      </div>
    </>
  );
};
