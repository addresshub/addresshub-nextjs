export default ({
  linesCount,
  setLinesCount,
  input,
  setInput,
  onClickConvert,
}) => {
  return (
    <form id="contact-form">
      <div className="row controls">
        <div className="col-12">
          <div className="input-group-meta form-group lg mb-40">
            <label>地址列表</label>
            <textarea
              value={input}
              onChange={(e) => {
                setInput(e.target.value);
                if (!e.target.value) {
                  setLinesCount(0);
                } else {
                  const lines = e.target.value?.split(/\r|\r\n|\n/);
                  setLinesCount(lines.length);
                }
              }}
              type="text"
            ></textarea>
            {linesCount > 0 && (
              <div className="invalid-feedback">{linesCount} address</div>
            )}
          </div>
        </div>

        <div className="col-12">
          <button
            type="button"
            className="theme-btn  theme-btn-primary"
            onClick={onClickConvert}
          >
            驗證
          </button>
        </div>
      </div>
    </form>
  );
};
