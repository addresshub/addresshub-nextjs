import AddressCleaningInput from "./address-cleaning-input";
import AddressCleaningTable from "./address-cleaning-table";

export default ({
  loading,
  list,
  setList,
  linesCount,
  setLinesCount,
  input,
  setInput,
  onClickConvert,
}) => {
  return (
    <>
      {!list && !loading && (
        <AddressCleaningInput
          linesCount={linesCount}
          setLinesCount={setLinesCount}
          input={input}
          setInput={setInput}
          onClickConvert={onClickConvert}
        />
      )}

      {list && (
        <AddressCleaningTable
          list={list}
          setList={setList}
          setInput={setInput}
          setLinesCount={setLinesCount}
          loading={loading}
        />
      )}
    </>
  );
};
