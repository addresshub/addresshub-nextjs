export default () => {
  return (
    <div className="fancy-short-banner-three mt-100 md-mt-150">
      <div className="container">
        <div className="bg-wrapper">
          <div className="row align-items-center">
            <div className="col-lg-6">
              <div className="title-style-one">
                <h6 className="font-rubik" style={{ color: "#787CFF" }}>
                  立即開始體驗
                </h6>

                <h2> 新用戶? 開始免費體驗</h2>
              </div>
              {/* /.title-style-one */}
            </div>
            <div className="col-lg-6">
              <div className="form-wrapper">
                {/* <FormEvent /> */}
                <p className="font-rubik">
                  已經註冊？
                  <a href="/login">登入</a>
                </p>
              </div>
              {/* /.form-wrapper */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
