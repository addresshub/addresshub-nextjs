export default () => {
  return (
    <footer className="theme-footer-two pt-100">
      <div className="top-footer">
        <div className="container">
          <div className="row justify-content-between">
            <div className="col-lg-4 col-12 footer-about-widget">
              <a
                href="/"
                className="logo"
                data-aos="fade-up"
                data-aos-duration="1200"
              >
                <img src="images/logo/logo.png" alt="" width={168} />
              </a>
            </div>
            {/* /.about-widget */}
            <div
              className="col-lg-2 col-md-4 footer-list"
              data-aos="fade-up"
              data-aos-duration="1200"
              data-aos-delay="50"
            >
              <h5 className="footer-title">服務</h5>
              <ul>
                <li>
                  <a href="/address-autocomplete">自動化完成地址輸入</a>
                </li>
                <li>
                  <a href="/address-cleaning">批量香港地址轉座標</a>
                </li>
                <li>
                  <a href="/address-geocoding">香港地址轉座標</a>
                </li>
                {/* <li>
                  <Link to="/">Address Routing</Link>
                </li> */}
              </ul>
            </div>
            {/* /.footer-list */}
            <div
              className="col-lg-2 col-md-4 footer-list"
              data-aos="fade-up"
              data-aos-duration="1200"
              data-aos-delay="100"
            >
              <h5 className="footer-title">關於我們</h5>
              <ul>
                {/* <li>
                  <Link to="/about-cs">About us</Link>
                </li> */}
                {/* <li>
                  <Link to="/faq-details">Faq Details</Link>
                </li> */}

                {/* <li>
                  <Link to="/pricing">{t("menu.pricing")}</Link>
                </li> */}
              </ul>
            </div>
            {/* /.footer-list */}
            <div
              className="col-lg-3 col-md-4 address-list"
              data-aos="fade-up"
              data-aos-duration="1200"
              data-aos-delay="150"
            >
              <h5 className="footer-title">聯絡我們</h5>
              {/* <p className="font-rubik">
                432 Melbourne Stadium Market <br />
                Melbourne , Australia
              </p> */}
              <ul className="info">
                <li>
                  <a href="mailto:okayfound@gmail.com">okayfound@gmail.com</a>
                </li>
                <li>
                  <a href="Tel: 91437908" className="mobile-num">
                    +852 9143 7908
                  </a>
                </li>
              </ul>
              {/* End ul */}
            </div>
            {/*  /.footer-list  */}
          </div>
        </div>
        {/* /.container */}
      </div>
      {/* /.top-footer */}

      <div className="container">
        <div className="bottom-footer-content">
          <div className="row">
            <div className="col-lg-8 ml-auto">
              <div className="d-md-flex align-items-center justify-content-between">
                <ul className="order-md-last">
                  <li>
                    <a href="/"></a>
                  </li>
                </ul>

                <p>
                  Copyright @{new Date().getFullYear()}{" "}
                  <a
                    href="https://okayfound.com"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    OkayFound Technology
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};
