import Header from "./header";
import Footer from "./footer";
// import useMemberStore from "../../../stores/useMemberStore";

export default function CommonLayout({ children }) {
  //   const { member } = useMemberStore();

  return (
    <div>
      <Header />
      {children}
      <Footer />
    </div>
  );
}
