import React, { useState } from "react";
import "react-pro-sidebar/dist/css/styles.css";
import {
  ProSidebar,
  SidebarHeader,
  Menu,
  MenuItem,
  SubMenu,
  SidebarContent,
} from "react-pro-sidebar";

const MegaMenuMobile = ({ t }) => {
  const Service = [
    {
      name: "批量香港地址轉座標",
      routerPath: "/address-cleaning",
    },
    {
      name: "自動化完成地址輸入",
      routerPath: "/address-autocomplete",
    },
    {
      name: "香港地址轉座標",
      routerPath: "/address-geocoding",
    },
  ];
  const Documents = [
    {
      name: "Address Autocomplete API",
      routerPath: "/documentation/autocomplete",
    },
    {
      name: "Address Geocoding API",
      routerPath: "/documentation/geocoding",
    },
  ];

  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);

  return (
    <div className="mega-menu-wrapper">
      <div className="mob-header multi-mob-header">
        <button className="toggler-menu" onClick={handleClick}>
          <div className={click ? "active" : ""}>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </button>
      </div>
      {/* End Header */}

      <ProSidebar
        className={click ? "mega-mobile-menu menu-open" : "mega-mobile-menu"}
      >
        <SidebarHeader>
          <div className="logo position-static">
            <a href="/">
              {/* <img src="/images/logo/deski_07.svg" alt="home" /> */}
              <img
                src="/images/logo/logo.png"
                alt="home"
                width={148}
                style={{ paddingTop: 36 }}
              />
            </a>
          </div>
          <div className="fix-icon text-dark" onClick={handleClick}>
            <img src="images/icon/close-w.svg" alt="icon" />
          </div>
          {/* Mobile Menu close icon */}

          {/* End logo */}
        </SidebarHeader>
        <SidebarContent>
          <Menu>
            <MenuItem>
              <a href="/">主頁</a>
            </MenuItem>

            <SubMenu title="服務" className="plus alt">
              {Service.map((val, i) => (
                <MenuItem key={i}>
                  <a href={val.routerPath}>{val.name}</a>
                </MenuItem>
              ))}
            </SubMenu>

            <SubMenu title="API 教學" className="plus alt">
              {Documents.map((val, i) => (
                <MenuItem key={i}>
                  <a href={val.routerPath}>{val.name}</a>
                </MenuItem>
              ))}
            </SubMenu>

            <MenuItem>
              <a href="/pricing">價錢</a>
            </MenuItem>

            {/* <MenuItem>
              <a href="/login">{t("menu.login")}</a>
            </MenuItem> */}
          </Menu>
        </SidebarContent>
      </ProSidebar>
    </div>
  );
};

export default MegaMenuMobile;
