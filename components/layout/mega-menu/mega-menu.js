import React from "react";

const MegaMenu = () => {
  return (
    <ul className="navbar-nav">
      <li className="nav-item  position-static">
        <a className="nav-link" href="/" data-toggle="dropdown">
          主頁
        </a>
      </li>
      <li className="nav-item  position-static">
        <a
          className="nav-link"
          href="/address-geocoding"
          data-toggle="dropdown"
        >
          香港地址轉座標
        </a>
      </li>
      <li className="nav-item  position-static">
        <a
          className="nav-link"
          href="/address-autocomplete"
          data-toggle="dropdown"
        >
          自動化地址輸入
        </a>
      </li>
      <li className="nav-item  position-static">
        <a className="nav-link" href="/address-cleaning" data-toggle="dropdown">
          批量地址轉座標
        </a>
      </li>

      {/* <li className="nav-item  position-static">
        <a className="nav-link" href="/pricing" data-toggle="dropdown">
          價錢
        </a>
      </li> */}
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
          API 教學
        </a>
        <ul className="dropdown-menu">
          {/* <li>
            <Link className="dropdown-item" to="/address-cleaning">
              Address Cleaning
            </Link>
          </li> */}
          <li>
            <a className="dropdown-item" href="/documentation/autocomplete">
              Address Autocomplete API
            </a>
          </li>
          <li>
            <a className="dropdown-item" href="/documentation/geocoding">
              Address Geocoding API
            </a>
          </li>
        </ul>
      </li>
      {/* <li className="nav-item  position-static">
        <a className="nav-link" href="#" data-toggle="dropdown">
          About Us
        </a>
      </li> */}
    </ul>
  );
};

export default MegaMenu;
