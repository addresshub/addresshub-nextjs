import { useState, useEffect } from "react";
import MegaMenu from "./mega-menu/mega-menu";
import MegaMenuMobile from "./mega-menu/mega-menu-mobile";
import { useRouter } from "next/router";
// import { BiLogIn, BiLogOut } from "react-icons/bi";

export default () => {
  const router = useRouter();
  const [navbar, setNavbar] = useState(false);

  const changeBackground = () => {
    if (window.scrollY >= 68) {
      setNavbar(true);
    } else {
      setNavbar(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", changeBackground);
    return () => window.removeEventListener("scroll", changeBackground);
  });

  return (
    <>
      <div
        className={
          navbar
            ? "theme-main-menu sticky-menu theme-menu-three bg-none fixed"
            : "theme-main-menu sticky-menu theme-menu-three bg-none"
        }
      >
        <div className="d-flex align-items-center justify-content-center">
          <div className="logo">
            <a href="/">
              <img
                src={`${router.basePath}/images/logo/logo.png`}
                width={148}
                style={{ paddingTop: navbar ? 16 : 0 }}
              />
            </a>
          </div>

          <nav id="mega-menu-holder" className="navbar navbar-expand-lg">
            <div className="container nav-container">
              <div
                className="navbar-collapse collapse"
                id="navbarSupportedContent"
              >
                <div className="d-lg-flex justify-content-between align-items-center">
                  <MegaMenu />
                  {/* End MegaMenu */}

                  {/* <ul className="right-widget user-login-button d-flex align-items-center justify-content-center">
                    <li className="d-sm-flex">
                      <ul className="language-button-group d-flex align-items-center justify-content-center">
                        {i18n.language !== "en" && (
                          <li>
                            <a
                              onClick={() => changeLanguageHandler("en")}
                              className="eng-lang active"
                            >
                              Eng
                            </a>
                          </li>
                        )}
                        {i18n.language === "en" && (
                          <li>
                            <a
                              onClick={() => changeLanguageHandler("zh-Hans")}
                              className="zh-Hans active"
                            >
                              中文
                            </a>
                          </li>
                        )}
                      </ul>
                    </li>
                    {!member && (
                      <li>
                        <Link
                          to="/login"
                          className="signIn-action d-flex align-items-center"
                        >
                          <BiLogIn style={{ fontSize: 22 }} />

                          <span>{t("menu.login")}</span>
                        </Link>
                      </li>
                    )}
                    {member && (
                      <li>
                        <Link
                          to="/logout"
                          className="signIn-action d-flex align-items-center"
                        >
                          <BiLogOut style={{ fontSize: 22 }} />

                          <span>{t("menu.logout")}</span>
                        </Link>
                      </li>
                    )}
                    <li>
                      <Link
                        to={member ? "/dashboard" : "/signup"}
                        className="signUp-action d-flex align-items-center"
                      >
                        <span>{t("menu.getting")}</span>
                        <img
                          src={`${window.location.origin}/images/icon/53.svg`}
                          alt="icon"
                        />
                      </Link>
                    </li>
                  </ul> */}
                  {/* End right-button-group  */}
                </div>
              </div>
            </div>
          </nav>
          {/* End nav */}
        </div>
        <MegaMenuMobile />
        {/* 	End Mega Menu for Mobile */}
      </div>
    </>
  );
};
