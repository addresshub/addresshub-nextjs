import Autocomplete from "react-autocomplete";
import { AiOutlineRight } from "react-icons/ai";

export default ({ input, setInput, list, setList, setResult }) => {
  return (
    <div className="fancy-hero-one geocoding-input-container">
      <div className="container">
        <div className="row">
          <div className="col-lg-9 m-auto">
            <h1 className="font-rubik" style={{ fontSize: 36 }}>
              自動化完成地址輸入
            </h1>
            <p className="sub-heading">
              自動化產生預測查詢地址，讓使用者快速輸入完整搜尋地址
            </p>
          </div>
        </div>

        <form className="search-form geocoding-input">
          <Autocomplete
            getItemValue={(item) => item.formatted_address}
            items={list}
            open={list && list.length > 0}
            renderItem={(item, isHighlighted) => (
              <div key={`${item.lat}_${item.lng}_${item.formatted_address}`}>
                <div
                  className="geocoding-select-item"
                  style={{
                    background: isHighlighted ? "rgb(245,245,245)" : "white",
                  }}
                >
                  <p className="geocoding-select-item-txt">
                    {item.formatted_address}
                  </p>
                  <AiOutlineRight size={12} />
                </div>
                <hr className="geocoding-select-hr" />
              </div>
            )}
            value={input}
            onChange={(e) => setInput(e.target.value)}
            inputProps={{
              placeholder: "開始輸入你的地址",
            }}
            wrapperStyle={{
              width: "100%",
            }}
            onSelect={(val, item) => {
              setList([]);
              setInput("");
              // setInput(item.formatted_address);
              setResult(item);
            }}
          />
        </form>
      </div>
    </div>
  );
};
