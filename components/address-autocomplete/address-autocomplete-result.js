import GoogleMapReact from "google-map-react";

export default ({ result }) => {
  const renderMarkers = (map, maps) => {
    let marker = new maps.Marker({
      position: { lat: parseFloat(result.lat), lng: parseFloat(result.lng) },
      map,
    });
    return marker;
  };

  return (
    <div className="container geocoding-result">
      <p className="pb-20">系統回應</p>
      <div className="mark-blue">
        <pre>{JSON.stringify(result, null, 2)}</pre>
      </div>

      <div className="map-area-one mt-170 md-mt-40">
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyBLtx_UJ7aEnWVYdAoiOufuwWhsT2hq72I" }}
          defaultCenter={{
            lat: result.lat,
            lng: result.lng,
          }}
          defaultZoom={14}
          onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
        >
          {/* <img
            lat={result.lat}
            lng={result.lng}
            src="./images/logo/location-pin.png"
            width={64}
            className="wrapper-marker"
          /> */}
        </GoogleMapReact>
      </div>
    </div>
  );
};
