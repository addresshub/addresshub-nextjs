import Autocomplete from "react-autocomplete";
import { AiOutlineRight } from "react-icons/ai";

function HomeAutocomplete({ input, setInput, list, setList, setResult }) {
  return (
    <div className="geocoding-input-container">
      <form className="search-form geocoding-input">
        <Autocomplete
          getItemValue={(item) => item.formatted_address}
          items={list}
          open={list && list.length > 0}
          renderItem={(item, isHighlighted) => (
            <div key={`${item.lat}_${item.lng}_${item.formatted_address}`}>
              <div
                className="geocoding-select-item"
                style={{
                  background: isHighlighted ? "rgb(245,245,245)" : "white",
                }}
              >
                <p className="geocoding-select-item-txt">
                  {item.formatted_address}
                </p>
                <AiOutlineRight size={12} />
              </div>
              <hr className="geocoding-select-hr" />
            </div>
          )}
          value={input}
          onChange={(e) => setInput(e.target.value)}
          inputProps={{
            placeholder: "開始輸入你的地址",
          }}
          wrapperStyle={{
            width: "100%",
          }}
          onSelect={(val, item) => {
            setList([]);
            setInput(item.formatted_address);
            setResult(item);
          }}
        />
      </form>
    </div>
  );
}

export default HomeAutocomplete;
