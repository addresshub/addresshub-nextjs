import BannerHome from "./banner-home";
import ServiceOffer from "./service-offer";
import ExploreServices from "./explore-services";
import FindYourAnswer from "./find-your-answer";
import HomeAutocomplete from "./autocomplete";

export {
  BannerHome,
  ServiceOffer,
  ExploreServices,
  FindYourAnswer,
  HomeAutocomplete,
};
