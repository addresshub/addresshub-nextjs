import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";

export default () => {
  const FaqContent = [
    {
      title: "地址查找覆蓋範圍包括什麼？",
      desc: "我們的地址查找涵蓋建築物和屋村、街道及村屋也包括在內。地址查詢API使用土地註冊處提供的數據源覆蓋100",
      expand: "a",
    },
    {
      title: "地址數據多久更新一次？",
      desc: "來自 data.gov(土地註冊處) 的地址每季度更新一次（1 月、4 月、7 月和 10 月）。",
      expand: "b",
    },
    {
      title: "AddressHub 與 Google 的 Autocomplete 比較如何？",
      desc: "Google 提供了地址自動填充和搜索服務。 \n\n 地址並非 100% 準確。 香港有許多新界屋村。 Google通常只搜尋到公屋／市區大廈。",
      expand: "c",
    },
    {
      title: "什麼是批量地址驗證?",
      desc: "向我們發送一份多餘文字、部分完整、格式不一致的地址列表，你會獲得一份經過清理後統一化的地址列表，其中包含與街道和郵政地址完全一致。",
      expand: "d",
    },
    {
      title: "我們如何處理大量的地址查找？",
      desc: "我們在性能調整和優化方面付出了很多努力，以確保通常可以在 0.1 秒（100 毫秒）內返回地址結果。 \n\n我們的地址查找 API 託管在分佈在多個地理數據中心的雲端，以提供冗餘和負載平衡。",
      expand: "e",
    },
  ];

  return (
    <div className="faq-classic pt-225 md-pt-120">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="title-style-one">
              <h6 className="font-rubik">尋找你的答案</h6>
              <h2>有什麼想法嗎？</h2>
            </div>
            {/* /.title-style-one */}
            {/* <Link to="/faq" className="theme-btn-one mt-50 md-mt-30">
              Go to Faq
            </Link> */}
          </div>
          {/* End .col */}
          <div className="col-lg-6">
            <div className="md-mt-60">
              <div className="accordion-style-four">
                <div className="faq-wrraper">
                  <Accordion preExpanded={[]} allowZeroExpanded>
                    {FaqContent.map((item, i) => (
                      <AccordionItem
                        className="card"
                        key={i}
                        uuid={item.expand}
                      >
                        <AccordionItemHeading className="card-header">
                          <AccordionItemButton>
                            <h5 className="mb-0">
                              <button className="btn btn-link">
                                {item.title}
                              </button>{" "}
                            </h5>
                          </AccordionItemButton>
                        </AccordionItemHeading>
                        {/* Accordion Heading */}
                        <AccordionItemPanel className="card-body fadeInUp">
                          <p style={{ whiteSpace: "pre-line" }}>{item.desc}</p>
                        </AccordionItemPanel>
                        {/* Accordion Body Content */}
                      </AccordionItem>
                    ))}
                  </Accordion>
                </div>
              </div>
            </div>
          </div>
          {/* End .col */}
        </div>
      </div>
    </div>
  );
};
