import HomeAutocomplete from "./autocomplete";

export default ({ t, input, setInput, list, setList, result, setResult }) => {
  return (
    <div className="hero-banner-three">
      <div className="container mb-40 pb-40">
        <div className="row">
          <div className="col-xl-9 col-lg-11 col-md-8 m-auto">
            <h1 className="font-rubik" style={{ fontSize: 36 }}>
              拆解難搞地址
            </h1>
          </div>
          {/* End .col */}

          <div className="col-xl-8 col-lg-9 m-auto">
            <p className="sub-text font-rubik">
              包含強大的校正能力、過濾多餘文字及括號、關鍵字搜尋地址
            </p>
          </div>
          {/* End .col */}
        </div>
        {/* End .row */}

        <HomeAutocomplete
          list={list}
          setList={setList}
          input={input}
          setInput={setInput}
          result={result}
          setResult={setResult}
        />
        {/* End search-filter-from */}

        <img
          src="images/media/hongkongmap.png"
          alt="illustration"
          className="illustration pb-80"
          style={{ width: "80%" }}
        />
      </div>

      {/* <img src="images/shape/68.svg" alt="shape" className="shapes shape-one" />
      <img src="images/shape/69.svg" alt="shape" className="shapes shape-two" />
      <img
        src="images/shape/70.svg"
        alt="shape"
        className="shapes shape-three"
      />
      <img
        src="images/shape/71.svg"
        alt="shape"
        className="shapes shape-four"
      />
      <img
        src="images/shape/72.svg"
        alt="shape"
        className="shapes shape-five"
      />
      <img src="images/shape/73.svg" alt="shape" className="shapes shape-six" />
      <img
        src="images/shape/74.svg"
        alt="shape"
        className="shapes shape-seven"
      />
      <img
        src="images/shape/75.svg"
        alt="shape"
        className="shapes shape-eight"
      />
      <img
        src="images/shape/76.svg"
        alt="shape"
        className="shapes shape-nine"
      />
      <img src="images/shape/77.svg" alt="shape" className="shapes shape-ten" /> */}
    </div>
  );
};
