export default () => {
  return (
    <div className="fancy-feature-six pt-120 pb-90 md-pt-80 md-pb-60 mt-140 md-mt-70">
      <div className="bg-wrapper">
        <div className="shapes shape-one"></div>
        <div className="shapes shape-two"></div>
        <div className="shapes shape-three"></div>
        <div className="shapes shape-four"></div>
        <div className="shapes shape-five"></div>
        <img
          src="images/shape/51.svg"
          alt="shape"
          className="shapes shape-six"
        />
        <div className="container">
          <div className="title-style-two text-center mb-85 md-mb-40">
            <h2>
              <span>
                尋找更多服務
                <img src="images/shape/line-shape-2.svg" alt="shape" />
              </span>
            </h2>
            <div className="sub-text mt-15">
              了解我們的地址解決方案以增強您的業務
            </div>
          </div>
          {/* End .title */}
          <div className="row justify-content-center">
            <div className="col-md-6" data-aos="fade-up" data-aos-delay={0}>
              <div className="block-style-thirtyTwo d-flex">
                <div className="icon d-flex align-items-center justify-content-center">
                  <img src={`images/icon/input.png`} alt="icon" />
                </div>
                <div className="text">
                  <h4>自動化完成地址輸入</h4>
                  <p>自動化產生預測查詢地址，讓使用者快速輸入完整搜尋地址</p>
                  <a href="/address-autocomplete" className="tran3s">
                    <img src="images/icon/135.svg" alt="icon" />
                  </a>
                </div>
              </div>
            </div>

            <div className="col-md-6" data-aos="fade-up" data-aos-delay={100}>
              <div className="block-style-thirtyTwo d-flex">
                <div className="icon d-flex align-items-center justify-content-center">
                  <img src="images/icon/csv.png" alt="icon" />
                </div>
                <div className="text">
                  <h4>批量香港地址轉座標</h4>
                  <p>批次輸入大量地址，一天內就可以完成數十萬地址定位</p>
                  <a href="/address-cleaning" className="tran3s">
                    <img src="images/icon/135.svg" alt="icon" />
                  </a>
                </div>
              </div>
            </div>

            <div className="col-md-6" data-aos="fade-up" data-aos-delay={0}>
              <div className="block-style-thirtyTwo d-flex">
                <div className="icon d-flex align-items-center justify-content-center">
                  <img src={`images/icon/location.png`} alt="icon" />
                </div>
                <div className="text">
                  <h4>香港地址轉座標</h4>
                  <p>
                    幫你拆開難搞地址，將地址過濾多餘文字、括號及其他格式統一校正，幫助系統定位目的地
                  </p>
                  <a href="/address-geocoding" className="tran3s">
                    <img src="images/icon/135.svg" alt="icon" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
