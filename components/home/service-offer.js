export default () => {
  return (
    <>
      {/* <ModalVideo
        channel="youtube"
        autoplay
        isOpen={isOpen}
        videoId="7e90gBu4pas"
        onClose={() => setOpen(false)}
      /> */}
      <div className="row align-items-center">
        <div className="col-lg-6 col-md-12 order-lg-last">
          <div className="text-wrapper pt-0">
            <div className="title-style-two mb-35 md-mb-20">
              <h2>
                <span>
                  大幅提升定位成功率
                  <img src="images/shape/line-shape-5.svg" alt="icon" />
                </span>
              </h2>
            </div>
            {/* <!-- /.title-style-two --> */}
            <p style={{ paddingRight: 40 }}>
              使用的地址資料庫多達數百萬條以上，包含現有大廈以及歷史資料，並且定期參考土地註冊處提供的資訊，進行更新，讓您得到最新、最準確的定位結果！
            </p>
          </div>
        </div>
        {/* End .col */}

        <div className="col-lg-6 col-md-12 order-lg-first">
          <div className="video-box">
            <img
              className="w-100"
              src="images/media/home-reduce.png"
              alt="reduce 80%"
            />
            <div
              // onClick={() => setOpen(true)}
              className="fancybox video-button d-flex align-items-center justify-content-center"
            >
              {/* <img src="images/icon/170.svg" alt="icon" /> */}
            </div>
          </div>
          {/* <!-- /.video-box --> */}
        </div>
      </div>
    </>
  );
};
