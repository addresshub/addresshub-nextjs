import Autocomplete from "react-autocomplete";
import { AiOutlineRight } from "react-icons/ai";

export default ({
  input,
  setInput,
  list,
  setList,
  onSearchAddress,
  loading,
  setResult,
}) => {
  return (
    <div className="fancy-hero-one geocoding-input-container">
      <div className="container">
        <div className="row">
          <div className="col-lg-9 m-auto">
            <h1 className="font-rubik" style={{ fontSize: 36 }}>
              香港地址轉座標
            </h1>
            <p className="sub-heading">
              幫你拆開難搞地址，將地址過濾多餘文字、括號及其他格式統一校正，幫助系統定位目的地
            </p>
          </div>
        </div>
        <form className="search-form geocoding-input">
          <Autocomplete
            getItemValue={(item) => item.label}
            items={list}
            open={list && list.length > 0}
            renderItem={(item, isHighlighted) => (
              <div key={`${item.lat}_${item.lng}_${item.formatted_address}`}>
                <div
                  className="geocoding-select-item"
                  style={{
                    background: isHighlighted ? "rgb(245,245,245)" : "white",
                  }}
                >
                  <p className="geocoding-select-item-txt">
                    {item.formatted_address} ({item.district})
                  </p>
                  <AiOutlineRight size={12} />
                </div>
                <hr className="geocoding-select-hr" />
              </div>
            )}
            value={input}
            onChange={(e) => setInput(e.target.value)}
            placeholder="Start typing your address"
            inputProps={{
              placeholder: "Start typing your address",
            }}
            wrapperStyle={{
              width: "100%",
            }}
            onSelect={(val, item) => {
              setList([]);
              setResult(item);
            }}
          />
          <button type="button" onClick={onSearchAddress}>
            {!loading && <img src="images/icon/47.svg" alt="icon" />}
            {loading && (
              <div
                className="spinner-border spinner-border-sm text-light"
                style={{ marginBottom: 2 }}
                role="status"
              >
                <span className="sr-only">Loading...</span>
              </div>
            )}
          </button>
        </form>
      </div>
      <div className="bubble-one"></div>
      <div className="bubble-two"></div>
      <div className="bubble-three"></div>
      <div className="bubble-four"></div>
      <div className="bubble-five"></div>
      <div className="bubble-six"></div>
    </div>
  );
};
