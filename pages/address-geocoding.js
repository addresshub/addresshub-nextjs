import { useState } from "react";
import axios from "axios";
import Head from "next/head";
import _ from "lodash";
import app from "../utils/app";
import CommonLayout from "../components/layout";
import {
  AddressGeocodingInput,
  AddressGeocodingResult,
} from "../components/address-geocoding";
import mock from "../utils/mock";

function AddressGeocoding() {
  const [loading, setLoading] = useState(false);
  const [list, setList] = useState([]);
  const [input, setInput] = useState(_.sample(mock));
  const [result, setResult] = useState();

  const fetchAddressRequest = async () => {
    setLoading(true);
    setList([]);
    setResult();
    axios
      .post(
        `${app.baseUrl}/geocoding`,
        {
          address: input,
        },
        {
          headers: {
            "API-KEY": "1810638996C0B05404154582ED692A57",
          },
        }
      )
      .then(function (response) {
        setList(
          response.data.result.lat
            ? [
                {
                  ...response.data.result,
                  label: response.data.result.formatted_address,
                },
              ]
            : []
        );
        setLoading(false);
      })
      .catch(function (error) {
        setLoading(false);

        console.log(error);
      });
  };

  const onSearchAddress = () => {
    fetchAddressRequest();
  };

  //   useEffect(() => {
  //     if (input && input.length > 1) fetchAddressRequest(input);
  //   }, [input]);
  return (
    <CommonLayout>
      <div className="main-page-wrapper">
        <Head>
          {/* <title>Address Geocoding | AddressHub</title> */}
          <title>香港地址轉座標</title>
          <meta
            name="description"
            content="幫你拆開難搞地址，將地址過濾多餘文字、括號及其他格式統一校正，可以幫助系統定位目的地。"
          />
          <meta
            name="og:description"
            property="og:description"
            content="幫你拆開難搞地址，將地址過濾多餘文字、括號及其他格式統一校正，可以幫助系統定位目的地。"
            key="ogdesc"
          />

          <meta
            name="keywords"
            content="香港地址,香港地址解析,地址轉座標,地址轉lat,中英翻譯地址,lat,lng"
          />
        </Head>
        <AddressGeocodingInput
          input={input}
          list={list}
          setList={setList}
          loading={loading}
          setInput={setInput}
          onSearchAddress={onSearchAddress}
          result={result}
          setResult={setResult}
        />

        {result && <AddressGeocodingResult result={result} />}
      </div>
    </CommonLayout>
  );
}

export default AddressGeocoding;
