import { useState } from "react";
import Head from "next/head";
import CommonLayout from "../components/layout";
import {
  BannerHome,
  ServiceOffer,
  ExploreServices,
  FindYourAnswer,
} from "../components/home";

export default function Home() {
  const [list, setList] = useState([]);
  const [input, setInput] = useState("");
  const [result, setResult] = useState();

  return (
    <CommonLayout>
      <Head>
        <title>拆解難搞地址 - 香港地址智能平台</title>
        <meta
          name="description"
          content="根據多年經驗自主開發的地址處理平台，拆解難搞地址及根據你傳入的地址準確定位。此外，更包含地址自動化完成及批量處理地址服務。"
        />
        <meta
          name="og:description"
          property="og:description"
          content="根據多年經驗自主開發的地址處理平台，拆解難搞地址及根據你傳入的地址準確定位。此外，更包含地址自動化完成及批量處理地址服務。"
          key="ogdesc"
        />
        <meta
          property="og:site_name"
          content="香港地址智能平台"
          key="ogsitename"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="main-page-wrapper">
        <BannerHome
          list={list}
          setList={setList}
          input={input}
          setInput={setInput}
          result={result}
          setResult={setResult}
        />
        <ServiceOffer />
        <ExploreServices />
        <FindYourAnswer />
      </div>
    </CommonLayout>
  );
}
