import React, { useState, useEffect } from "react";
import axios from "axios";
import Head from "next/head";
import CommonLayout from "../../components/layout";
import app from "../../utils/app";

function DocumentAutocomplete() {
  const [response, setResponse] = useState();

  const fetchAutocomplete = () => {
    axios
      .get(`${app.baseUrl}/autocomplete?s=海怡`, {
        headers: {
          "API-KEY": "1810638996C0B05404154582ED692A57",
        },
      })
      .then(function (response) {
        setResponse(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => fetchAutocomplete(), []);

  return (
    <CommonLayout>
      <div className="main-page-wrapper">
        <Head>
          <title>Documentation | AddressHub</title>
        </Head>
        <div className="doc-container doc-box mt-70 sm-m0">
          <div className="container">
            <div className="col-xl-12 doc-main-body">
              <h3>Autocomplete API Documentation</h3>
              <p>
                Use the autocomplete API to validate and autocomplete addresses
                when user input
              </p>
              <ul className="list-item-one">
                <li>Get Request</li>
              </ul>
              <div className="mark-blue">
                <pre>https://api.addresshub.hk/autocomplete?s=海怡</pre>
              </div>
              <h3>cURL Request</h3>
              <pre className="bg-black">
                curl -X GET --header 'Accept: application/json' --header
                'API-KEY: demo-api-key'
                'https://api.addresshub.hk/autocomplete?s=海怡'
              </pre>
              <h3>Request Parameters</h3>
              <table className="request-param-table">
                <thead>
                  <tr>
                    <th>Parameter</th>
                    <th>Description</th>
                    <th>Type</th>
                    <th>Required</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>s</td>
                    <td>
                      Search criteria / address fragment to query (minimum of 2
                      characters)
                    </td>
                    <td>string</td>
                    <td>Yes</td>
                  </tr>
                  <tr>
                    <td>max(Coming Soon)</td>
                    <td>
                      Number of addresses to return (default = 10, max = 50)
                    </td>
                    <td>integer</td>
                    <td>No</td>
                  </tr>
                  <tr>
                    <td>showStreet(Coming Soon)</td>
                    <td>Whether show street data in result (default false)</td>
                    <td>boolean</td>
                    <td>No</td>
                  </tr>
                </tbody>
              </table>
              <h3>Response</h3>'
              {response && (
                <div className="mark-blue">
                  <pre>{JSON.stringify(response, null, 2)}</pre>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </CommonLayout>
  );
}

export default DocumentAutocomplete;
