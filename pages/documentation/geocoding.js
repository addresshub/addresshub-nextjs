import React, { useEffect, useState } from "react";
import axios from "axios";
import CommonLayout from "../../components/layout";
import Head from "next/head";
import app from "../../utils/app";

function DocumentAutocomplete() {
  const [response, setResponse] = useState();

  const fetchGeoCoding = () => {
    axios
      .post(
        `${app.baseUrl}/geocoding`,
        {
          address: "海怡半島",
        },
        {
          headers: {
            "API-KEY": "1810638996C0B05404154582ED692A57",
          },
        }
      )
      .then(function (response) {
        setResponse(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => fetchGeoCoding(), []);

  return (
    <CommonLayout>
      <div className="main-page-wrapper">
        <Head>
          <title>Documentation | AddressHub</title>
        </Head>
        <div className="doc-container doc-box mt-70 sm-m0">
          <div className="container">
            <div className="col-xl-12 doc-main-body">
              <h3>Geocoding API Documentation</h3>
              <p>Add latitude and longitude coordinates to your address data</p>
              <ul className="list-item-one">
                <li>POST Request</li>
              </ul>
              <div className="mark-blue">
                <pre>https://api.addresshub.hk/geocoding?address=海怡半島</pre>
              </div>

              <h3>cURL Request</h3>

              <pre className="bg-black">
                curl -X POST --header 'Accept: application/json' --header
                'API-KEY: demo-api-key'
                'https://api.addresshub.hk/geocoding?address=海怡半島'
              </pre>

              <h3>Request Parameters</h3>
              <table className="request-param-table">
                <thead>
                  <tr>
                    <th>Parameter</th>
                    <th>Description</th>
                    <th>Type</th>
                    <th>Required</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>address</td>
                    <td>
                      Search criteria / address fragment to query (minimum of 2
                      characters)
                    </td>
                    <td>String</td>
                    <td>Yes</td>
                  </tr>
                  {/* <tr>
                <td>max</td>
                <td>Number of addresses to return (default = 10, max = 50)</td>
                <td>integer</td>
                <td>No</td>
              </tr> */}
                  {/* <tr>
                <td>showStreet</td>
                <td>Whether show street data in result (default false)</td>
                <td>boolean</td>
                <td>No</td>
              </tr> */}
                </tbody>
              </table>

              <h3>Response</h3>
              {response && (
                <div className="mark-blue">
                  <pre>{JSON.stringify(response, null, 2)}</pre>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </CommonLayout>
  );
}

export default DocumentAutocomplete;
