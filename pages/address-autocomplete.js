import { useState, useEffect } from "react";
import Head from "next/head";
import axios from "axios";
import CommonLayout from "../components/layout";
import app from "../utils/app";
import {
  AddressAutocompleteInput,
  AddressAutocompleteResult,
} from "../components/address-autocomplete";

export default () => {
  const [list, setList] = useState([]);
  const [input, setInput] = useState("");
  const [result, setResult] = useState();

  const onSearchAddress = async (input) => {
    setList([]);
    axios
      .get(`${app.baseUrl}/autocomplete?s=${input}`, {
        headers: {
          "API-KEY": "1810638996C0B05404154582ED692A57",
        },
      })
      .then(function (response) {
        setList(response.data.list);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    if (input && input.length > 1) {
      onSearchAddress(input);
    }
  }, [input]);

  return (
    <CommonLayout>
      <div className="main-page-wrapper">
        <Head>
          <title>自動化完成地址輸入</title>

          <meta
            name="description"
            content="香港地址資料庫，搜尋全港所有大廈及屋苑。自動完成輸入，可以加快完成您／您的客戶輸入搜尋地址，自動化系統會產生預測查詢字串，讓使用者能快速輸入完整搜尋字詞，進而節省時間。"
          />

          <meta
            name="og:description"
            property="og:description"
            content="香港地址資料庫，搜尋全港所有大廈及屋苑。自動完成輸入，可以加快完成您／您的客戶輸入搜尋地址，自動化系統會產生預測查詢字串，讓使用者能快速輸入完整搜尋字詞，進而節省時間。"
            key="ogdesc"
          />

          <meta
            name="keywords"
            content="香港地址,自動完成地址,香港地址資料庫,hong kong address autocomplete,autocomplete"
          />
        </Head>
        <AddressAutocompleteInput
          input={input}
          list={list}
          setList={setList}
          setInput={setInput}
          result={result}
          setResult={setResult}
        />

        {result && <AddressAutocompleteResult result={result} />}
      </div>
    </CommonLayout>
  );
};
