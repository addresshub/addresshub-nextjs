import React, { useState } from "react";
import _ from "lodash";
import Head from "next/head";
import CommonLayout from "../components/layout";
import mock from "../utils/mock";
import axios from "axios";
import toast from "react-hot-toast";
import app from "../utils/app";
import { AddressCleaningIntro } from "../components/address-cleaning";

export default () => {
  const [linesCount, setLinesCount] = useState(4);
  const [list, setList] = useState(null);
  const [input, setInput] = useState(_.sampleSize(mock, 4).join("\n"));
  const [loading, setLoading] = useState(false);

  const fetchAddressRequest = async (address) => {
    return new Promise(function (resolve, reject) {
      axios
        .post(
          `${app.baseUrl}/geocoding`,
          {
            address,
          },
          {
            headers: {
              "API-KEY": "1810638996C0B05404154582ED692A57",
            },
          }
        )
        .then(function (response) {
          resolve({ ...response.data.result, key: address });
        })
        .catch(function (error) {
          console.log(error);
        });
    });
  };

  const onClickConvert = async () => {
    if (linesCount === 0) {
      toast.error("Please input address");
      return;
    } else if (linesCount > 10) {
      toast.error("Maximum address limit of 10");
      return;
    }

    setList([]);
    const lines = input?.split(/\r|\r\n|\n/);
    if (lines.length > 0) {
      setLoading(true);
      for (let i = 0; i < lines.length; i++) {
        const result = await fetchAddressRequest(lines[i]);
        console.log(result);
        setList((state) => {
          state = state ? [...state, result] : [result];
          return state;
        });
      }
      setLoading(false);
    }
  };

  return (
    <CommonLayout>
      <div className="main-page-wrapper">
        <Head>
          <title>批量香港地址轉座標</title>
          <meta
            name="description"
            content="批次傳入大量地址，讓您一天內就能完成數十萬地址，輕鬆轉化為CSV檔案，地址有街道／大廈名錯字也能快速定位，大幅提升定位成功率！"
          />
          <meta
            name="og:description"
            property="og:description"
            content="批次傳入大量地址，讓您一天內就能完成數十萬地址，輕鬆轉化為CSV檔案，地址有街道／大廈名錯字也能快速定位，大幅提升定位成功率！"
            key="ogdesc"
          />

          <meta
            name="keywords"
            content="香港地址,香港地址批量,地址批量轉換,地址轉lat,lat,lng"
          />
        </Head>
        <AddressCleaningIntro
          input={input}
          setInput={setInput}
          list={list}
          setList={setList}
          linesCount={linesCount}
          loading={loading}
          setLinesCount={setLinesCount}
          onClickConvert={onClickConvert}
        />

        {/* <AddressCleaningTrial /> */}
      </div>
    </CommonLayout>
  );
};
