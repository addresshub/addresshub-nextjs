import apisauce from "apisauce";
import appConfig from "./app";

const API_URL = appConfig.baseUrl;

export default class API {
  constructor(token) {
    this.api = apisauce.create({
      baseURL: API_URL,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      timeout: 25000,
    });

    this.api.axiosInstance.interceptors.response.use(
      function (response) {
        return response;
      },
      function (error) {
        return Promise.reject(error);
      }
    );
  }

  async fetchMemberSecurity() {
    const url = "/member/fetchMemberSecurity";
    const res = await this.api.post(url, {});
    return res;
  }

  async updateMemberSecurity(data) {
    const url = "/member/updateMemberSecurity";
    const res = await this.api.post(url, data);
    return res;
  }
}
